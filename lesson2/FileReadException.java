package lesson2;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class FileReadException {
    public static void main (String[] args) {

        try (FileInputStream fis =  new FileInputStream("test11.txt")) {
            byte[] bytes = fis.readAllBytes();
//            String text = readFile("test11.txt");
            System.out.println(new String(bytes));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            System.out.println("Файл не был найден!");
        } catch (IOException e) {
            System.out.println("Файл поврежден!");
//            e.printStackTrace();
//        } finally {
//            if(fis != null) {
//                try {
//                    fis.close();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
        }

    }

//    private static String readFile (String fileName) throws FileNotFoundException, IOException {
//        FileInputStream fis;
//        fis = new FileInputStream(fileName);
//        byte[] bytes = fis.readAllBytes();
//        return new String(bytes);
//    }

}
