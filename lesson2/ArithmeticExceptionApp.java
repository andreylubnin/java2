package lesson2;

public class ArithmeticExceptionApp {

    public static void main (String[] args) {
        try {
            int a = 0;
            int b = divide(a);
            System.out.println("Result: " + b);
            int[] arr = {1, 2, 3};
            arr[42] = 99;
//        } catch (ArithmeticException e) {
//            System.out.println("Деление на ноль!");
//        } catch (ArrayIndexOutOfBoundsException e) {
//            System.out.println("Ошибка обращения к индексу массива!");
//        } catch (Exception e) {
//            System.out.println("Ошибка ");
//            e.printStackTrace();
//        }
//        } catch (ArithmeticException | ArrayIndexOutOfBoundsException e) {
//            System.out.println(e.getMessage());
//        }
        } catch (DivideByZeroException e) {
            System.out.println(e.getMessage());
        }

        System.out.println("finish!");
    }

    private static int divide (int a) throws DivideByZeroException {
        if(a == 0) {
            throw new DivideByZeroException();
        }
        return 10 / a;
    }
}
