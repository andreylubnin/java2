package lesson1.part3;

import lesson1.part2.Color;

public class TestRiver {
    public static void main (String[] args) {
        River river = new River("Волга", 100);

        Waterfowl duck = new Duck("Утка");
        DogV2 dog = new DogV2("Шарик", "Black", "Shepherd");
        Pet cat = new CatV2("Мурзик", Color.BLACK);

        Waterfowl[] waterfowls = {duck, dog};
        swim(river, duck, dog);
    }

    private static void swim (River river, Waterfowl... waterfowls) {
        for (Waterfowl waterfowl : waterfowls) {
            System.out.println(waterfowl);
            System.out.println(river.doSwim(waterfowl));
        }
    }


}
