package lesson1.part3;

public interface Waterfowl {

    int DEFAULT_SWIM_DISTANCE = 50;

    int swim();
}
