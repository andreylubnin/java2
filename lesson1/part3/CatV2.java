package lesson1.part3;

import lesson1.part2.Animal;
import lesson1.part2.Color;

public class CatV2 extends Animal implements Pet {
    private Color color = Color.RED;
    private CatAttribute catAttribute;

    @Override
    public void loveMaster () {
        System.out.println("Мурр!");
    }

    @Override
    public boolean isUseful () {
        return false;
    }

    public static class CatAttribute {
        private String meal;
        private int weight;
        private Color colorEyes;
    }

    public CatV2 (String name, Color color, CatAttribute catAttribute) {
        super(name);
        this.catAttribute = catAttribute;
        this.color = color;
    }

    public CatV2 (String name, Color color) {
        super(name);
        this.color = color;
    }

    @Override
    public void animalInfo () {
        super.animalInfo();
        System.out.println("Cat name is " + super.getName() + "; color = " + color);
    }

    @Override
    public void voice () {
        System.out.println("Мяу!");
    }

    public CatAttribute getCatAttribute () {
        return catAttribute;
    }
}
