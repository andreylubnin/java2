package lesson1.part2;

public class TestAnimal {
    public static void main (String[] args) {
//        for(Color value: Color.values()) {
//            System.out.println(value.getRussianColor());
//        }

        Cat.CatAttribute catAttribute = new Cat.CatAttribute();
        Animal cat = new Cat("Баааарсикк", Color.WHITE, catAttribute);
        Animal dog = new Dog("Мухтар", "black", "Shepherd");

        ((Cat) cat).getCatAttribute();
        System.out.println("instanceof: " + (cat instanceof Cat));

        infoAndJump(cat);
        infoAndJump(dog);

    }

    private static void infoAndJump(Animal animal) {
        animal.animalInfo();
        animal.jump();
        animal.voice();

        System.out.println();
    }
}
