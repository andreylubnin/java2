package lesson2_hw;

public class MyArraySizeException extends ArrayIndexOutOfBoundsException {

    public MyArraySizeException () {
        super("Неверный размер заданного массива!");
    }

}
