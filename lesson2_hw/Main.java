package lesson2_hw;

import java.lang.reflect.Array;
import java.text.NumberFormat;
import java.util.Arrays;

import static java.lang.Integer.parseInt;

public class Main {
    private static String[][] arr;
    private final static int CORRECT_ARRAY_ROWS = 4;
    private final static int CORRECT_ARRAY_COLS = 4;


    public static void main (String[] args) {
        arr = new String[][]{
                {"1", "2", "3", "17"},
                {"7", "0", "1", "1"},
                {"8", "1", "2r", "3"},
                {"2", "3", "11", "22"}};

        processArray(arr);

    }

    public static void processArray (String[][] array) {

        checkArray(array);
        sumArray(array);

    }

    public static void sumArray (String[][] array) {
        int sumArray = 0;
        for (String[] strings : array) {
            int rowIndex = Arrays.asList(array).indexOf(strings);
            for (String cell : strings) {
                int colIndex = Arrays.asList(strings).indexOf(cell);
                if(cell.matches("[0-9]+")) {
                    sumArray += parseInt(cell);
                } else {
                    System.out.printf("Ошибка при подсчете суммы элементов массива по адресу: [%s,%s]. Заданный элемент '%s' не является числом.%n", rowIndex + 1, colIndex + 1, cell);
                    throw new MyArrayDataException();
                }
            }
        }
        System.out.printf("Сумма элементов массива: %s.%n",sumArray);
    }

    private static void checkArray (String[][] array) {
        int arrCells = 0;
        int arrRows = 0;
        arrRows += arr.length;
        for (String[] strings : array) {
            arrCells += strings.length;
        }
        if (arrRows != CORRECT_ARRAY_ROWS || arrCells != CORRECT_ARRAY_COLS * CORRECT_ARRAY_ROWS) {
            throw new MyArraySizeException();
        }
        System.out.println("Заданный массив имеет корректный размер.");
    }
}
