package lesson2_hw;

public class MyArrayDataException extends NumberFormatException {
    public MyArrayDataException() {
        super("Ошибка преобразования типа, элемент массива не является числом. ");
    }
}
