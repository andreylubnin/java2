package lesson1_hw;

public class Obstacle {
    private final String title;
    protected final int length;

    public Obstacle (String title, int length) {
        this.title = title;
        this.length = length;
    }

    @Override
    public String toString () {
        return "Препятствие{" +
                "title='" + title + '\'' +
                ", length=" + length +
                '}';
    }

    public String getTitle () {
        return title;
    }

    public int getLength () {
        return length;
    }
}
