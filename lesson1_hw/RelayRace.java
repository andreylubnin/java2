package lesson1_hw;

import lesson1_hw.Interfaces.*;

import java.util.ArrayList;

public class RelayRace {

    private static ArrayList<Object> competitors = new ArrayList<>();
    public static boolean flag_not_finished = false;

    public static void main (String[] args) {
        Wall wall1 = new Wall("Стена1", 15);
        Wall wall2 = new Wall("Стена2", 60);
        Wall wall3 = new Wall("Стена3", 70);

        TreadMill dor1 = new TreadMill("Беговая дорожка 1", 500);
        TreadMill dor2 = new TreadMill("Беговая дорожка 2", 150);
        TreadMill dor3 = new TreadMill("Беговая дорожка 3", 600);
        TreadMill dor4 = new TreadMill("Беговая дорожка 4", 1500);

        Obstacle[] obstacles = {wall1, dor1, wall2, dor2, wall3, dor3, dor4};

        competitors.add(new Cat("Мурзик"));
        competitors.add(new Cat("Барсик"));
        competitors.add(new Cat("Чубайс"));
        competitors.add(new Cat("Мурка"));
        competitors.add(new Robot("Робот1"));
        competitors.add(new Robot("Робот2"));
        competitors.add(new Robot("Робот3"));
        competitors.add(new Robot("Робот4"));
        competitors.add(new Human("Мишка"));
        competitors.add(new Human("Петька"));
        competitors.add(new Human("Федька"));
        competitors.add(new Human("Сашка"));

        runRace(competitors, obstacles);
    }

    public static void runRace(ArrayList<Object> competitors, Obstacle... obstacles) {
        for (Object competitor : competitors) {
            System.out.println("--------------------------------" + System.lineSeparator() + "На соревнование выходит " + competitor.toString() + ".");
            flag_not_finished = false;
            int i = 0;
            for (Obstacle prep : obstacles) {
                if(flag_not_finished) break;
                System.out.print("Впереди " + prep.getTitle() + ((prep instanceof Wall) ? " высотой " : " длиной " ) + prep.getLength() + " м. ");
                if(prep instanceof Wall) {
                    ((Wall) prep).doJump((IJump)competitor);
                } else if(prep instanceof TreadMill) {
                    ((TreadMill) prep).doRun((IRun)competitor);
                }
                i++;
                if(i>=obstacles.length) {
                    System.out.println(competitor.toString() + " успешно прошел эстафету и переходит на новый этап соревнований! Наши поздравления!");
                }
            }
            System.out.println("--------------------------------" + System.lineSeparator() + "< ...ждем следующего участника... >");
        }
    }
}
