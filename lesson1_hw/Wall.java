package lesson1_hw;

import lesson1_hw.Interfaces.IJump;

public class Wall extends Obstacle{

    public Wall (String title, int length) {
        super(title, length);
    }

    public boolean doJump(IJump jumper) {
        int jumpLength = jumper.jump();
        if (jumpLength < length) {
            System.out.println(" и сходит с дистанции...");
            RelayRace.flag_not_finished = true;
        } else {
            System.out.println(" и продолжает соревноваться!");
        }
        return jumpLength >= length;
    }

}
