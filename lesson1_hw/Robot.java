package lesson1_hw;

import lesson1_hw.Interfaces.IJump;
import lesson1_hw.Interfaces.IRun;

public class Robot implements IRun, IJump {
    public static final int RUN_DISTANCE = 100;
    public static final int JUMP_DISTANCE = 20;
    private String name;

    public Robot (String name) {
        this.name = name;
    }

    public String getName () {
        return name;
    }

    public void setName (String name) {
        this.name = name;
    }

    @Override
    public int jump () {
        System.out.print(name + " прыгает на " + JUMP_DISTANCE + " м... ");
        return JUMP_DISTANCE;
    }

    @Override
    public int run () {
        System.out.print(name + " бежит на " + RUN_DISTANCE + " м... ");
        return RUN_DISTANCE;
    }


    @Override
    public String toString () {
        return "робот модели '" + name + '\'';
    }
}
