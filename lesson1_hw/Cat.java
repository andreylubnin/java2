package lesson1_hw;

import lesson1_hw.Interfaces.IJump;
import lesson1_hw.Interfaces.IRun;

public class Cat implements IRun, IJump {
    public static final int RUN_DISTANCE = 3000;
    public static final int JUMP_DISTANCE = 100;
    private String name;

    public Cat (String name) {
        this.name = name;
    }

    public String getName () {
        return name;
    }

    public void setName (String name) {
        this.name = name;
    }

    @Override
    public int jump () {
        System.out.print(name + " прыгает на " + JUMP_DISTANCE + " м... ");
        return JUMP_DISTANCE;
    }

    @Override
    public int run () {
        System.out.print(name + " бежит на " + RUN_DISTANCE + " м... ");
        return RUN_DISTANCE;
    }


    @Override
    public String toString () {
        return "кот с кличкой '" + name + '\'';
    }
}
