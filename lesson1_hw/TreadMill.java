package lesson1_hw;

import lesson1_hw.Interfaces.IRun;

public class TreadMill extends Obstacle{

    public TreadMill (String title, int length) {
        super(title, length);
    }

    public boolean doRun(IRun runner) {
        int runLength = runner.run();
        if (runLength < length) {
            System.out.println(" и сходит с дистанции...");
            RelayRace.flag_not_finished = true;
        } else {
            System.out.println(" и продолжает соревноваться!");
        }
        return runLength >= length;
    }

}
